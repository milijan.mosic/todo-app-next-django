## 1

```sh
git clone https://gitlab.com/milijan.mosic/todo-app-next-django.git
cd todo-app-next-django/
bash gen-env-files.sh
sudo echo "127.0.0.1 todo-app.dev.next" >> /etc/hosts
sudo echo "127.0.0.1 todo-api.dev.django" >> /etc/hosts
docker-compose up --build --remove-orphans
```

## 2

2nd terminal

```sh
bash init-dj.sh
```

## 3

Now, visit "todo-api.dev.django/admin", login with your created user and make an API KEY. Then, open '.env' file in www/ dir. and paste it there.

## 4

Rerun

```sh
docker-compose up --build --remove-orphans
```
