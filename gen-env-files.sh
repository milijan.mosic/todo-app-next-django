#/bin/bash


env_api_path='./api/config/'
env_www_path='./www/'
current_path=<pwd

got_user_input=false
while [ $got_user_input == false ]
do
  pass=false
  while [ $pass == false ]
  do
    echo -e "Enter database name:\n>>> "
    read db_name

    length=${#db_name}
    if [ $length -ge 2 ]; then
      pass=true
      clear
    else
      clear
      echo -e "\n\nInput is not equal or greater than 2 characters or is an empty input... try again.\n\n"
    fi
  done

  pass=false
  while [ $pass == false ]
  do
    echo -e "Enter database username:\n>>> "
    read db_username

    length=${#db_username}
    if [ $length -ge 2 ]; then
      pass=true
      clear
    else
      clear
      echo -e "\n\nInput is not equal or greater than 2 characters or is an empty input... try again.\n\n"
    fi
  done

  pass=false
  while [ $pass == false ]
  do
    echo -e "Enter database password:\n>>> "
    read db_password

    length=${#db_password}
    if [ $length -ge 5 ]; then
      pass=true
      clear
    else
      clear
      echo -e "\n\nInput is not equal or greater than 5 characters or is an empty input... try again.\n\n"
    fi
  done
done


cd env_api_path
echo "SECRET_KEY=django-insecure-$(head -c 32 /dev/urandom | base64)" > .env
echo "DEBUG=True" >> .env
echo "DB_NAME=$db_name" >> .env
echo "DB_USER=$db_username" >> .env
echo "DB_PASS=$db_password" >> .env
echo "DB_HOST=db" >> .env
echo "DB_PORT=5432" >> .env

cd current_path
cd env_www_path
echo "API_KEY=" > .env

cd current_path
echo "DB_NAME=$db_name" > .env
echo "DB_USER=$db_username" >> .env
echo "DB_PASS=$db_password" >> .env
